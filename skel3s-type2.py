# 3states: A <> B <> C
casino = {
	'states': ('A', 'B', 'C'),
	'observations': ('1','2','3','4'),
	'start_probability': {'A':0.6, 'B':0.3, 'C':0.1},
	'transition_probability': {
		'A':{'A':0.9, 'B':0.1, 'C':0.0},
		'B':{'A':0.1, 'B':0.8, 'C':0.1},
		'C':{'A':0.0, 'B':0.1, 'C':0.9},
	},
	'emission_probability':{
		'A':{'1':0.5,'2':0.5,'3':0.0,'4':0.0},
		'B':{'1':1.0/4,'2':1.0/4,'3':1.0/4,'4':1.0/4},
		'C':{'1':0.1,'2':0.1,'3':0.1,'4':0.7},
	},
}

casino_log = {
	'states': ('A', 'B', 'C'),
	'observations': ('1','2','3','4'),
	'start_probability': {'A':log(1.0/3), 'B':log(1.0/3), 'C':log(1.0/3)},
	'transition_probability': {
		'A':{'A':log(0.5), 'B':log(0.5), 'C':log(0.0)},
		'B':{'A':log(0.25), 'B':log(0.5), 'C':log(0.25)},
		'C':{'A':log(0.0), 'B':log(0.5), 'C':log(0.5)},
	},
	'emission_probability':{
		'A':{'1':log(0.3),'2':log(0.3),'3':log(0.2),'4':log(0.2)},
		'B':{'1':log(1.0/4),'2':log(1.0/4),'3':log(1.0/4),'4':log(1.0/4)},
		'C':{'1':log(0.2),'2':log(0.2),'3':log(0.2),'4':log(0.4)},
	},
}