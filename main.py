from mykov import sim
from hmmpytk.hmmpytk import hmm_faster, hmm
from math import log,exp
import sys, os
from multiprocessing import Pool
from skel10s import casino, casino_log 

def finishing_msg(msg):
	print msg

def run_train(ob_len, ob_num, obs_list, iter, delta, prefix, suffix, dp=False, epsilon=1.0):
	model = hmm.HMM()
	model.read_from_file("tmp_model")

	model.set_verbose(1)

	model.log_to_file("%s%dx%d%s"%(prefix, ob_len, ob_num, suffix))
	if dp:
		model.set_dp(True)
		model.set_dp_epsilon(epsilon)
		model.set_sensitivity(ob_len)
		# model.set_sensitivity(ob_len/float(ob_num))
		# model.set_sensitivity(1)
	model.train_multiple(obs_list, iter, delta)
	model.print_readable_matrices()
	model.close_logging()
	model.write_to_file("%s%dx%d_model%s"%(prefix, ob_len, ob_num, suffix))

	return "\t%dx%d%s :: Finish!"%(ob_len, ob_num, suffix)

def run_compare(ob_len, ob_num, prefix='', iter=80, delta=0.000001, epsilon=1.0, multithread=False):
# def run_compare(ob_len, ob_num, prefix='', iter=100, delta=0.000001, epsilon=1.0, multithread=True):
	model = hmm.HMM()
	model.set_states(casino['states'])
	model.set_observations(casino['observations'])

	datas = [sim.runHMM(casino, ob_len) for i in xrange(ob_num)]
	# print "Test set %dx%d, max iteration%d, delta=%.8f, epsilon=%.2f"%(ob_len, ob_num, iter, delta, epsilon)
	stdout_bak = sys.stdout
	sys.stdout = open("%s%dx%d_training_data.txt"%(prefix,ob_len, ob_num), 'w')
	for d in datas:
		for c in d[0]:
			sys.stdout.write(c)
		print ''
		for c in d[1]:
			sys.stdout.write(c)
		print ''
	sys.stdout.close()
	sys.stdout = stdout_bak
	obs_list = [datas[i][0] for i in xrange(ob_num)]

	model.randomize_matrices()
	# model.set_emission_matrix(casino_log['emission_probability'])
	# model.set_initial_matrix(casino_log['start_probability'])
	# model.set_transition_matrix(casino_log['transition_probability'])
	model.write_to_file("tmp_model")

	if multithread:
		p = Pool(processes=8)
		r1 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_100', True, 100), callback=finishing_msg)
		r2 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_50', True, 50), callback=finishing_msg)	
		r3 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_10', True, 10), callback=finishing_msg)
		r4 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_5', True, 5), callback=finishing_msg)
		r5 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_1', True, 1), callback=finishing_msg)
		r6 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_0_5', True, 0.5), callback=finishing_msg)	
		r7 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_0_1', True, 0.1), callback=finishing_msg)
		r8 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '', False, 1), callback=finishing_msg)
		p.close()
		p.join()

	else:
		run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '', False, 1)
		sys.stdout = stdout_bak
		# epsilon = [100,50,10,5,1,0.5,0.1]
		epsilon = [1,0.5,0.1]
		for e in epsilon:
			print "\t: %s"%str(e)
			run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '_e_%s'%str(e), True, e)
			sys.stdout = stdout_bak
		# print "\t.1"
		# run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '')
		# sys.stdout = stdout_bak

		# print "\t.2"
		# run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '_dp', True, epsilon)
		# sys.stdout = stdout_bak

		# print "\t.3"
		# run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '_dp_edi', True, epsilon/iter)
		# sys.stdout = stdout_bak

def run_compare_epsilon(ob_len, ob_num, prefix='', iter=100, delta=0.000001, multithread=True):
	model = hmm.HMM()
	model.set_states(casino_log['states'])
	model.set_observations(casino_log['observations'])

	datas = [sim.runHMM(casino, ob_len) for i in xrange(ob_num)]
	# print "Test set %dx%d, max iteration%d, delta=%.8f, epsilon=%.2f"%(ob_len, ob_num, iter, delta, epsilon)
	stdout_bak = sys.stdout
	sys.stdout = open("%s%dx%d_training_data.txt"%(prefix,ob_len, ob_num), 'w')
	for d in datas:
		for c in d[0]:
			sys.stdout.write(c)
		print ''
		for c in d[1]:
			sys.stdout.write(c)
		print ''
	sys.stdout.close()
	sys.stdout = stdout_bak
	obs_list = [datas[i][0] for i in xrange(ob_num)]

	model.randomize_matrices()
	model.write_to_file("tmp_model")

	if multithread:
		p = Pool(processes=4)
		r1 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_5', True, 5), callback=finishing_msg)
		r2 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_0_5', True, 0.5), callback=finishing_msg)	
		r3 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_0_25', True, 0.25), callback=finishing_msg)
		r4 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, '_e_0_1', True, 0.1), callback=finishing_msg)
		p.close()
		p.join()

if __name__ == '__main__':
	count = 1
	N = 10 		#      number of iter per test 
	test_set = []

	# newer test set
	test_set += [(10,100),(10,200),(10,300)]			# 10
	test_set += [(20,100),(20,200),(20,300)]			# 20
	test_set += [(30,100),(30,200),(30,300)]			# 30
	test_set += [(10,600),(10,1200)]			# 10-2
	test_set += [(20,600),(20,1200)]			# 20-2
	test_set += [(30,600),(30,1200)]			# 30-2

	# new test set that normalize all length to have same sensitivity
	# test_set += [(10,100),(20,50),(32,32),(40,25),(50,20)]			# 1000
	# test_set += [(20,200),(40,100),(64,64),(80,50),(100,40)]			# 4000
	# test_set += [(30,300),(60,150),(95,95),(120,75),(150,60)]			# 9000
	# test_set += [(40,400),(80,200),(127,127),(160,100),(200,80)]		# 16000			
	# test_set += [(50,500),(100,250),(158,158),(200,125),(250,100)]	# 25000		
	# test_set += [(60,600),(120,300),(190,190),(240,150),(300,120)]	# 36000	
	# test_set += [(80,800),(160,400),(253,253),(320,200),(400,160)]	# 64000	
	# test_set += [(110,1100),(220,550),(348,348),(440,275),(550,220)]	# 121000	

	# old test set
	# test_set += [(25,40),(50,20),(100,10),(200,5),(500,2)]
	# test_set += [(25,80),(50,40),(100,20),(200,10),(500,4)]
	# test_set += [(25,200),(50,100),(100,50),(200,25),(500,10)]
	# test_set += [(50,200),(100,100),(200,50),(400,25),(500,20)]
	# test_set += [(50,400),(100,200),(200,100),(400,50),(500,40)]
	# test_set += [(50,800),(100,400),(200,200),(400,100),(500,80)]
	# test_set += [(100,800),(200,400),(400,200),(800,100)]

	for t in test_set:
		prefix = str(t[0]*t[1])
		print "TEST %s#"%prefix + "%dx%d"%t
		for i in xrange(N):
			print "%d/%d"%(i,N)
			run_compare(t[0], t[1],  prefix + "_" + str(count) + "_" + str(i) + "_")#, 100, 0.1, 1.0, True)
			# run_compare_epsilon(t[0], t[1], prefix + "_" + str(count) + "_" + str(i) + "_")
		count = count + 1
		count = 1 if count > 5 else count