from mykov import sim
from hmmpytk.hmmpytk import hmm
import sys, os
from math import log
from main import casino
from mykov import util

folder_prefix = "result/combine-3s-2/last/30/"
output = "verify/"
diff = True

def compare(str1, str2):
	if len(str1) != len(str2):
		return 0

	correct = 0
	for i in xrange(len(str1)):
		if str1[i] == str2[i]:
			correct += 1

	return correct/float(len(str1))

def verify(ob_len, data, prefix, suffix, N):
	model = hmm.HMM()
	sum = 0
	model.read_from_file(folder_prefix + "%s%dx%d%s"%(prefix, ob_len[0],ob_len[1],suffix))
	for ob in data:
		s1,s2 = model.viterbi(ob[0]),ob[1]
		result = compare(s1,s2)
		sum += result

	return float(sum)/len(data)

if __name__ == "__main__":
	v_datas = [sim.runHMM(casino, 100) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 200) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 300) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 400) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 500) for i in xrange(10)]
	# v_datas += [sim.runHMM(casino, 400) for i in xrange(10)]
	# v_datas += [sim.runHMM(casino, 800) for i in xrange(10)]
	# v_datas += [sim.runHMM(casino, 1600) for i in xrange(10)]
	
	vlen = len(v_datas)+1

	test_set = []
	# newer test set
	# test_set += [(10,100),(10,200),(10,300)]			# 10
	# test_set += [(10,600),(10,1200)]			# 10-2
	# test_set += [(20,100),(20,200),(20,300)]			# 20
	# test_set += [(20,600),(20,1200)]			# 20-2
	test_set += [(30,100),(30,200),(30,300)]			# 30
	test_set += [(30,600),(30,1200)]			# 30-2


	# test_set += [(127,127)]
	# new test set that normalize all length to have same sensitivity
	# test_set += [(10,100),(20,50),(32,32),(40,25),(50,20)]			# 1000
	# test_set += [(20,200),(40,100),(64,64),(80,50),(100,40)]			# 4000
	# test_set += [(30,300),(60,150),(95,95),(120,75),(150,60)]			# 9000
	# test_set += [(40,400),(80,200),(127,127),(160,100),(200,80)]		# 16000			
	# test_set += [(50,500),(100,250),(158,158),(200,125),(250,100)]	# 25000		
	# test_set += [(60,600),(120,300),(190,190),(240,150),(300,120)]	# 36000	
	# test_set += [(80,800),(160,400),(253,253),(320,200),(400,160)]	# 64000	
	# test_set += [(110,1100),(220,550),(348,348),(440,275),(550,220)]	# 121000	

	# test_set += [(10,100), (10, 200), (10,300)]
	# test_set += [(20,100), (20, 200), (20,300)]
	# test_set += [(30,100), (30, 200), (30,300)]

	# test_case = ["_model_e_5","_model_e_0_5","_model_e_0_25","_model_e_0_1",]
	# test_case = ["_model_e_0_001","_model_e_0_005",]
	# test_case = ["_model","_model_e_1","_model_e_0_1","_model_e_0_01",]
	test_case = ["_model","_model_e_100","_model_e_50","_model_e_10","_model_e_5","_model_e_1","_model_e_0_5","_model_e_0_1",]
	# test_case = ["_model","_model_e_100","_model_e_50","_model_e_10","_model_e_5","_model_e_1","_model_e_0.5","_model_e_0.1",]
	# test_case = [100,50,10,5,1,0.5,0.1]
	# test_case = ["_model_e_1","_model_e_0_1","_model_e_0_01",]

	# f = open(folder_prefix + output + "%d_test_set.txt"%(test_set[0][0]*test_set[0][1]), "w")
	# counter = 0
	# for line in v_datas:
	# 	f.write("%d\n"%counter)
	# 	for c in line[0]:
	# 		f.write(c)
	# 	f.write("\n")
	# 	for c in line[1]:
	# 		f.write(c)
	# 	f.write("\n")
	# 	counter += 1
	# f.close()

	N = 10
	loop = 1
	num_case = 3



	bak = sys.stdout
	for t in test_set:
		if loop == 1:
			# print header
			print ""
			for s in test_case:
				print ",%s"%s,
		print "\n%sx%s"%t,
		
		n_datas = [[]]*N
		for c in test_case:
			sum_accuracy = 0.0
			for i in xrange(N):
				if c == "_model" and diff:
					m = hmm.HMM()
					m.read_from_file(folder_prefix + "%s%dx%d%s"%(str(t[0]*t[1]) + "_" + str(loop) + "_" + str(i) + "_", t[0],t[1],c))
					n_datas[i] = []
					for pair in v_datas:
						n_datas[i] += [(pair[0],m.viterbi(pair[0]))]
				
				elif diff:
					sum_accuracy += verify(t, n_datas[i], str(t[0]*t[1]) + "_" + str(loop) + "_" + str(i) + "_", c, N)
				else:
					sum_accuracy += verify(t, v_datas, str(t[0]*t[1]) + "_" + str(loop) + "_" + str(i) + "_", c, N)
			print ",%.4f"%(sum_accuracy/10),
		loop %= 5
		loop += 1
		