from hmmpytk.hmmpytk import hmm
import sys
import shutil

folder_in = "result/combine-3s-2/ev/30/"
folder_out = "result/combine-3s-2/last/30/"

def load_and_add_noise(ob_len, ob_num, prefix, suffix, epsilon):
	stdout_bak = sys.stdout

	model = hmm.HMM()
	model.read_from_file("%s%s%dx%d_model"%(folder_in, prefix, ob_len, ob_num))
	model.set_verbose(1)
	model.log_to_file("%s%s%dx%d%s"%(folder_out, prefix, ob_len, ob_num, suffix))
	model.set_dp_epsilon(epsilon)	#####
	model.set_sensitivity(ob_len)
	model.add_noise_tran_matrix()
	model.print_readable_matrices()
	model.close_logging()
	model.write_to_file("%s%s%dx%d_model%s"%(folder_out, prefix, ob_len, ob_num, suffix))

	sys.stdout = stdout_bak

if __name__ == '__main__':
	count = 1
	N = 10 		#      number of iter per test set

	# old test set
	# test_set = [(50,200),(100,100),(200,50),(400,25),(500,20)]	
	# test_set = [(50,400),(100,200),(200,100),(400,50),(500,40)]
	# test_set = [(25,200),(50,100),(100,50),(200,25),(500,10),]
	# test_set = [(25,80),(50,40),(100,20),(200,10),(500,4),]
	# test_set = [(25,40),(50,20),(100,10),(200,5),(500,2),]
	# test_set = [(50,800),(100,400),(200,200),(400,100),(500,80)]
	# test_set = [(100,800),(200,400),(400,200),(800,100)]

	test_set = []
	# new test set that normalize all length to have same sensitivity
	# test_set += [(10,100),(20,50),(32,32),(40,25),(50,20)]			# 1000
	# test_set += [(20,200),(40,100),(64,64),(80,50),(100,40)]			# 4000
	# test_set += [(30,300),(60,150),(95,95),(120,75),(150,60)]			# 9000
	# test_set += [(40,400),(80,200),(127,127),(160,100),(200,80)]		# 16000			
	# test_set += [(50,500),(100,250),(158,158),(200,125),(250,100)]	# 25000		
	# test_set += [(60,600),(120,300),(190,190),(240,150),(300,120)]	# 36000	
	# test_set += [(80,800),(160,400),(253,253),(320,200),(400,160)]	# 64000	

	# test_set += [(10,100),(10,200),(10,300)]			# 10
	# test_set += [(10,600),(10,1200)]			# 10-2
	# test_set += [(20,100),(20,200),(20,300)]			# 20
	# test_set += [(20,600),(20,1200)]			# 20-2
	test_set += [(30,100),(30,200),(30,300)]			# 30
	test_set += [(30,600),(30,1200)]			# 30-2

	# varying epsilon
	epsilon = [
		('_e_100', 	100.0),
		('_e_50', 	50.0),
		('_e_10', 	10.0),
		('_e_5', 	5.0),
		('_e_1', 	1.0),
		('_e_0_5', 	0.5),
		# ('_e_0_2', 	0.2),
		('_e_0_1', 0.1),
	]

	for t in test_set:
		prefix = str(t[0]*t[1])
		print "TEST %s#"%prefix + "%dx%d"%t
		for i in xrange(N):
			print "%d/%d"%(i,N)
			shutil.copyfile("%s%s%dx%d_model"%(folder_in, prefix + "_" + str(count) + "_" + str(i) + "_", t[0], t[1]), "%s%s%dx%d_model"%(folder_out, prefix + "_" + str(count) + "_" + str(i) + "_", t[0], t[1]))
			for e in epsilon:
				load_and_add_noise(t[0], t[1], prefix + "_" + str(count) + "_" + str(i) + "_", e[0], e[1])
		count = count + 1
		count = 1 if count > 5 else count
