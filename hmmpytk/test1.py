from hmmpytk import hmm_faster
'''
states = ['F', 'L']
observations = ['1','2','3','4','5','6']
start_probability = {'F':0.7, 'L':0.3}
transition_probability = {
	'F':{'F':0.95, 'L':0.05},
	'L':{'F':0.1, 'L':0.9},
}
emission_probability = {
	'F':{'1':1.0/6,'2':1.0/6,'3':1.0/6,'4':1.0/6,'5':1.0/6,'6':1.0/6},
	'L':{'1':0.1,'2':0.1,'3':0.1,'4':0.1,'5':0.1,'6':0.5},
}
'''

#model = hmm_faster.HMM(states, observations, states, transition_probability, emission_probability)

model = hmm_faster.HMM()
model.set_verbose(True)
model.read_from_file('./lame_tagger/model2.hmm')
