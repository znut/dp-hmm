import entropy
from hmmpytk.hmmpytk import hmm
from numpy import array
import math

folder_prefix = "result/4-30/"

def L2(mat):
	mat1 = mat[0]
	mat2 = mat[1]
	sum = 0
	for i in xrange(len(mat1)):
		sum += (mat1[i]-mat2[i])*(mat1[i]-mat2[i])
	return math.sqrt(sum)

def prepare_mat(mat):
	r_mat = []
	for st_i in mat:
		in_mat = []
		for st_j in mat[st_i]:
			in_mat.append(math.exp(mat[st_i][st_j]))
			# print st_i, st_j
		r_mat.append(in_mat)
	return r_mat

if __name__ == "__main__":
	p = hmm.HMM()
	q = hmm.HMM()

	CSV = True

	test_set = [(50,200),(100,100),(200,50),(400,25),(500,20)]
	# test_set = [ (50,400),(100,200),(200,100),(400,50),(500,40) ]
	# test_set += [(50,800),(100,400),(200,200),(400,100),(500,80)]
	# test_set += [(100,800),(200,400),(400,200),(800,100)]
	N = 5
	loop = 1
	for t in test_set:
		amt = t[0]*t[1]
		for i in xrange(N):
			p.read_from_file("result/4-30/%d_%d_%d_%s_model"%(amt,loop,i,"%dx%d"%(t[0],t[1])))
			q.read_from_file("result/4-30/%d_%d_%d_%s_model_minusone"%(amt,loop,i,"%dx%d"%(t[0],t[1])))

			p_trans = prepare_mat(p.get_transition_matrix())
			q_trans = prepare_mat(q.get_transition_matrix())
			p_em = prepare_mat(p.get_emission_matrix())
			q_em = prepare_mat(q.get_emission_matrix())
			p_ini = [math.exp(p.get_initial_matrix()[x]) for x in p.get_initial_matrix()]
			q_ini = [math.exp(q.get_initial_matrix()[x]) for x in q.get_initial_matrix()]

			if CSV:
				print "%dx%d,"%(t[0],t[1]),
				print entropy.jensen_shannon_divergence(array([p_ini,q_ini]))[0],
				print ",",
				for i in xrange(len(p_trans)):
					print entropy.jensen_shannon_divergence(array([p_trans[i],q_trans[i]]))[0],
					print ",",
				for i in xrange(len(p_em)):
					print entropy.jensen_shannon_divergence(array([p_em[i],q_em[i]]))[0],
					print ",",
				print entropy.relative_entropy(array(p_ini),array(q_ini))[0],
				print ",",
				for i in xrange(len(p_trans)):
					print entropy.relative_entropy(array(q_trans[i]),array(p_trans[i]))[0],
					print ",",
				for i in xrange(len(p_em)):
					print entropy.relative_entropy(array(q_em[i]),array(p_em[i]))[0],
					print ",",
				print L2([p_ini,q_ini]),
				print ",",
				for i in xrange(len(p_trans)):
					print L2([q_trans[i],p_trans[i]]),
					print ",",
				for i in xrange(len(p_em)):
					print L2([q_em[i],p_em[i]]),
					print ",",
				print ""
			# print "JSD"
			# print "  Initial"
			# print entropy.jensen_shannon_divergence(array([p_ini,q_ini]))[0]
			# print "  Transition"
			# for i in xrange(len(p_trans)):
				# print entropy.jensen_shannon_divergence(array([p_trans[i],q_trans[i]]))[0]
			# print "  Emission"
			# for i in xrange(len(p_em)):
				# print entropy.jensen_shannon_divergence(array([p_em[i],q_em[i]]))[0]

			# print "Dkl"
			# print "  Initial"
			# print entropy.relative_entropy(array(p_ini),array(q_ini))[0]
			# print "  Transition"
			# for i in xrange(len(p_trans)):
				# print entropy.relative_entropy(array(q_trans[i]),array(p_trans[i]))[0]
			# print "  Emission"
			# for i in xrange(len(p_em)):
				# print entropy.relative_entropy(array(q_em[i]),array(p_em[i]))[0]

			# print "L2"
			# print "  Initial"
			# print L2([p_ini,q_ini])
			# print "  Transition"
			# for i in xrange(len(p_trans)):
				# print L2([q_trans[i],p_trans[i]])
			# print "  Emission"
			# for i in xrange(len(p_em)):
				# print L2([q_em[i],p_em[i]])

		loop += 1