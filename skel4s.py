from math import log

# 4state: all transition
casino = {
	'states': ('A', 'B', 'C', 'D'),
	'observations': ('1','2','3','4'),
	'start_probability': {'A':0.25, 'B':0.25, 'C':0.25, 'D':0.25},
	'transition_probability': {
		'A':{'A':0.9, 	'B':0.05, 	'C':0.0, 	'D':0.05},
		'B':{'A':0.05,	'B':0.9, 	'C':0.05, 	'D':0.0},
		'C':{'A':0.0,	'B':0.05, 	'C':0.9, 	'D':0.05},
		'D':{'A':0.05, 	'B':0.0, 	'C':0.9, 	'D':0.05},
	},
	'emission_probability':{
		'A':{'1':1.0,	'2':0.0,	'3':0.0,	'4':0.0},
		'B':{'1':0.0,	'2':1.0,	'3':0.0,	'4':0.0},
		'C':{'1':0.0,	'2':0.0,	'3':1.0,	'4':0.0},
		'D':{'1':0.0,	'2':0.0,	'3':0.0,	'4':1.0},
	},
}

st_prob_log = {}
for k in casino['start_probability'].keys():
	st_prob_log[k] = log(casino['start_probability'][k])

trans_prob_log = {}
for k1 in casino['transition_probability'].keys():
	trans_prob_log[k1] = {}
	for k2 in casino['transition_probability'][k1].keys():
		trans_prob_log[k1][k2] = log(casino['transition_probability'][k1][k2])

emiss_prob_log = {}
for k1 in casino['emission_probability'].keys():
	emiss_prob_log[k1] = {}
	for k2 in casino['emission_probability'][k1].keys():
		emiss_prob_log[k1][k2] = log(casino['emission_probability'][k1][k2])

casino_log = {
	'states': casino['states'],
	'observations': casino['observations'],
	'start_probability': st_prob_log,
	'transition_probability': trans_prob_log,
	'emission_probability': emiss_prob_log,
}