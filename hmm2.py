from mykov import sim
from hmmpytk.hmmpytk import hmm_faster, hmm
from math import log,exp
import sys, os
from multiprocessing import Pool

casino = {
	'states': ('F', 'L'),
	'observations': ('1','2','3','4','5','6'),
	'start_probability': {'F':0.7, 'L':0.3},
	'transition_probability': {
		'F':{'F':0.95, 'L':0.05},
		'L':{'F':0.1, 'L':0.9},
	},
	'emission_probability':{
		'F':{'1':1.0/6,'2':1.0/6,'3':1.0/6,'4':1.0/6,'5':1.0/6,'6':1.0/6},
		'L':{'1':0.1,'2':0.1,'3':0.1,'4':0.1,'5':0.1,'6':0.5},
		# 'L':{'1':0.04,'2':0.04,'3':0.04,'4':0.04,'5':0.04,'6':0.8},
	},
}

casino_log = {
	'states': ('F', 'L'),
	'observations': ('1','2','3','4','5','6'),
	'start_probability': {'F':log(0.7), 'L':log(0.3)},
	'transition_probability': {
		'F':{'F':log(0.95), 'L':log(0.05)},
		'L':{'F':log(0.1), 'L':log(0.9)},
	},
	'emission_probability':{
		'F':{'1':log(1.0/6),'2':log(1.0/6),'3':log(1.0/6),'4':log(1.0/6),'5':log(1.0/6),'6':log(1.0/6)},
		'L':{'1':log(0.1),'2':log(0.1),'3':log(0.1),'4':log(0.1),'5':log(0.1),'6':log(0.5)},
	},	
}

def finishing_msg(msg):
	print msg

def run_train(ob_len, ob_num, obs_list, iter, delta, prefix, suffix, dp=False, epsilon=1.0):
	model = hmm.HMM()
	model.read_from_file("tmp_model")

	model.set_verbose(1)

	model.log_to_file("%s%dx%d%s"%(prefix, ob_len, ob_num, suffix))
	if dp:
		model.set_dp(True)
		model.set_dp_epsilon(epsilon)
	model.train_multiple(obs_list, iter, delta)
	model.print_readable_matrices()
	model.close_logging()
	model.write_to_file("%s%dx%d_model%s"%(prefix, ob_len, ob_num, suffix))

	return "\t%dx%d%s :: Finish!"%(ob_len, ob_num, suffix)

def run_compare(ob_len, ob_num, prefix='', iter=100, delta=0.000001, epsilon=1.0, multithread=True):
	model = hmm.HMM()
	model.set_states(casino_log['states'])
	model.set_observations(casino_log['observations'])

	datas = [sim.runHMM(casino, ob_len) for i in xrange(ob_num)]
	# print "Test set %dx%d, max iteration%d, delta=%.8f, epsilon=%.2f"%(ob_len, ob_num, iter, delta, epsilon)
	stdout_bak = sys.stdout
	sys.stdout = open("%s%dx%d_training_data.txt"%(prefix,ob_len, ob_num), 'w')
	for d in datas:
		for c in d[0]:
			sys.stdout.write(c)
		print ''
		for c in d[1]:
			sys.stdout.write(c)
		print ''
	sys.stdout.close()
	sys.stdout = stdout_bak
	obs_list = [datas[i][0] for i in xrange(ob_num)]

	model.randomize_matrices()
	model.write_to_file("tmp_model")

	if multithread:
		p = Pool(processes=3)
		r1 = p.apply_async(run_train, (ob_len, ob_num, obs_list, iter, delta, prefix, ''), callback=finishing_msg)
		r2 = p.apply_async(run_train, (ob_len, ob_num, obs_list[:-1], iter, delta, prefix, '_minusone'), callback=finishing_msg)	
		# r3 = p.apply_async(run_train,(ob_len, ob_num, obs_list, iter, delta, prefix, '_dp_edi', True, epsilon/iter), callback=finishing_msg)
		p.close()
		p.join()

	else:
		return False
		print "\t.1"
		run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '')
		sys.stdout = stdout_bak

		print "\t.2"
		run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '_dp', True, epsilon)
		sys.stdout = stdout_bak

		print "\t.3"
		run_train(ob_len, ob_num, obs_list, iter, delta, prefix, '_dp_edi', True, epsilon/iter)
		sys.stdout = stdout_bak

if __name__ == '__main__':
	count = 1
	N = 5 		#      number of iter per test set
	# # 10000 epsilon/iter
	# test_set = [(50,200),(100,100),(200,50),(400,25),(500,20)]
	# prefix = "10000"
	# for t in test_set:
	# 	print "TEST #%s"%str(t)
	# 	for i in xrange(N):
	# 		run_compare(t[0], t[1],  prefix + "_" + str(count) + "_" + str(i) + "_") #, 100, 0.1, 1.0, True)
	# 	count = count + 1

	# 20000 epsilon/iter
	# test_set = [(50,400),(100,200),(200,100),(400,50),(500,40)]
	# prefix = "20000"
	# for t in test_set:
	# 	print "TEST #%d"%count
	# 	for i in xrange(N):
	# 		run_compare(t[0], t[1], prefix + "_" + str(count) + "_" + str(i) + "_")
	# 	count = count + 1
	
	count = 1
	# 40000 epsilon/iter
	test_set = [(50,800),(100,400),(200,200),(400,100),(500,80)]
	prefix = "40000"
	for t in test_set:
		print "TEST #%d"%count
		for i in xrange(N):
			run_compare(t[0], t[1],  prefix + "_" + str(count) + "_" + str(i) + "_")
		count = count + 1
		
	count = 1
	# 80000 epsilon/iter
	test_set = [(100,800),(200,400),(400,200),(800,100)]
	prefix = "80000"
	for t in test_set:
		print "TEST #%d"%count
		for i in xrange(N):
			run_compare(t[0], t[1],  prefix + "_" + str(count) + "_" + str(i) + "_")
		count = count + 1
