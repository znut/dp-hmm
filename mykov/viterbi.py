import math, sys
import util
v = {}
ptr = {}

def init(model):
	for s in model['states']:
		for e in model['observations']:
			v[s+e] = {}
			ptr[s+e] = {}

def getv(s,i):
	if i < 0:
		return 1
	try:
		return v[s][i]
	except KeyError:
		return 0

def viterbi(model, observation):
	init(model)
	n = len(observation)
	state = ''

	for i in range(n):
		# print '>>> %d' %i
		for s in model['states']:
			# print '>> ' + s
			sn = s + observation[i]
			v[sn][i] = model['emission_probability'][s][observation[i]] 
			maxp = 0
			maxs = ''
			for s0 in model['states']:
				s0n = s0 + observation[i-1]
				prob = getv(s0n,i-1)
				
				if i == 0:
					prob *= model['start_probability'][s]
					maxs = 'B'
				else:
					prob *= model['transition_probability'][s0][s]
				# print 'v-1*a: ' + str(prob)
				
				if prob > maxp:
					maxp = prob
					maxs = s0n
			# print maxp, v[sn][i]
			# v[sn][i] = math.log(maxp) + math.log(v[sn][i])
			v[sn][i] *= maxp
			# print v[sn][i]
			ptr[sn][i] = maxs

	# util.print_array(v, n)

	# termination
	maxv = 0
	lasts = ''
	for e in model['observations']:
		for s in model['states']:
			v1 = getv(s+e,i) 
			if v1 > maxv:
				maxv = v1
				lasts = s+e

	# print lasts

	# traceback
	stack = []
	for i in reversed(range(n)):
		stack += lasts[0]
		lasts = ptr[lasts][i]

	# return v
	return reversed(stack)
