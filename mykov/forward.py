import util
class ForwordAlg:
	def __init__(self, model, seq):
		self.model = model
		self.seq = seq
		self.pstates = []
		self.f = {}
		for s in model['states']:
			for e in model['observations']:
				self.f[s+e] = {}

	def get_f(self, state, i):
		if i < 0:
			return 1
		# try:
		return self.f[state][i]
		# except KeyError:
			# return 0

	def forward(self):
		i = 0
		for c in self.seq:
			maxp = 0
			pstate = ''
			for s in self.model['states']:
				count = 0
				for s0 in self.model['states']:
					count += self.get_f(s0+self.seq[i-1], i-1) * self.model['transition_probability'][s0][s]
				self.f[s+c][i] = self.model['emission_probability'][s][c] * count
				if self.f[s+c][i] > maxp:
					maxp = self.f[s+c][i]
					pstate = s
			self.pstates.append(pstate)
			i += 1

		# util.print_array(self.f, len(self.seq))
		return self.pstates