import random
import sys

DEBUG = True

casino = {
	'states': ('F', 'L'),
	'observations': ('1','2','3','4','5','6'),
	'start_probability': {'F':0.5, 'L':0.5},
	'transition_probability': {
		'F':{'F':0.95, 'L':0.05},
		'L':{'F':0.1, 'L':0.9},
	},
	'emission_probability':{
		'F':{'1':1.0/6,'2':1.0/6,'3':1.0/6,'4':1.0/6,'5':1.0/6,'6':1.0/6},
		'L':{'1':0.1,'2':0.1,'3':0.1,'4':0.1,'5':0.1,'6':0.5},
	},
}

states = ('Rainy', 'Sunny')
 
observations = ('walk', 'shop', 'clean')
 
start_probability = {'Rainy': 0.6, 'Sunny': 0.4}
 
transition_probability = {
   'Rainy' : {'Rainy': 0.7, 'Sunny': 0.3},
   'Sunny' : {'Rainy': 0.4, 'Sunny': 0.6},
   }
 
emission_probability = {
   'Rainy' : {'walk': 0.1, 'shop': 0.4, 'clean': 0.5},
   'Sunny' : {'walk': 0.6, 'shop': 0.3, 'clean': 0.1},
   }

def print_debug(msg):
	if DEBUG:
		print '[DBG]' + msg

def randoutput(distribution):
	r = random.SystemRandom().random()
	count = 0
	for e in distribution:
		if r < distribution[e] + count:
			return e
		count = count + distribution[e]

def runHMM(model, nloop):
	states_log = []
	emission = []
	#--- init
	state = randoutput(model['start_probability'])

	states_log += state
	emission += str(randoutput(model['emission_probability'][state]))

	#--- run
	for i in xrange(nloop-1):
		#-- changestate
		state = randoutput(model['transition_probability'][state])
		#-- emit
		emission += str(randoutput(model['emission_probability'][state]))
		
		# print_debug(state)
		states_log += state

	return (emission,states_log)


if __name__ == '__main__':
	runHMM(casino, 50)
	print ''