import sys

def printline(string):
	for c in string:
		sys.stdout.write(c)
	print ''

def print_array(arr, n, fl=False):
	for k in arr:
		print '\t' + k + '\t',
	print ''
	for i in range(n):
		print str(i) + ':\t',
		for k in arr:
			try:
				if fl:
					print '%.10f\t' % arr[k][i],
				else:
					print arr[k][i],
					print '\t\t',
			except KeyError:
				print 0,
				print '\t\t',
		print ''
	print ''