from mykov import sim
from hmmpytk.hmmpytk import hmm
import sys, os
from main import casino

folder_prefix = "result/5-16/lastit/"
output = "verify/"

filter_trapped = False

def print_inline(string):
	for c in string:
		sys.stdout.write(c)
	print ''

def compare(str1, str2):
	if len(str1) != len(str2):
		return 0

	correct = 0
	for i in xrange(len(str1)):
		if str1[i] == str2[i]:
			correct += 1

	return correct/float(len(str1))

def verify(ob_len, data, prefix, suffix, N):
	model = hmm.HMM()

	sum = 0
	model.read_from_file(folder_prefix + "%s%dx%d%s"%(prefix, ob_len[0],ob_len[1],suffix))
	out_str = ""
	result_arr = []
	for ob in data:
		result = compare(model.viterbi(ob[0]),ob[1])
		# result2 = result if result > 0.5 else 1-result
		result2 = result
		result_arr.append((result,result2))
		# print "\tCorrectness:\t%.4f"%result
		# out_str += ",Correctness:,%.4f,%0.4f,,,\n"%(result,result2)
		sum +=result2
	# print "Average:\t%.4f"%(sum/len(data))
	# out_str += "Average:,%.4f,,,,,\n"%(sum/len(data))
	
	if sum/len(data) < 0.75 and filter_trapped:
		trapped = 1
	else:
		trapped = 0

	for r in result_arr:
		# out_str += ",,,,,,\n"
		out_str += ",Correctness:,%.4f,%0.4f,%d,,\n"%(r[0],r[1],trapped)
	# out_str += ",,,,,,\n"
	out_str += "Average:,%.4f,,,,,\n"%(sum/len(data))
	return out_str.split('\n')

if __name__ == "__main__":
	v_datas = [sim.runHMM(casino, 100) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 200) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 400) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 800) for i in xrange(10)]
	v_datas += [sim.runHMM(casino, 1600) for i in xrange(10)]
	
	vlen = len(v_datas)+1

	# test_set = [(50,200),(100,100),(200,50),(400,25),(500,20), (50,400),(100,200),(200,100),(400,50),(500,40), (50,800),(100,400),(200,200),(400,100),(500,80)]
	# test_set = [(50,200),(100,100),(200,50),(400,25),(500,20)]
	# test_set = [ (50,400),(100,200),(200,100),(400,50),(500,40), ]
	# test_set = [(50,800),(100,400),(200,200),(400,100),(500,80)]
	# test_set = [(100,800),(200,400),(400,200),(800,100)]

	# small data set
	test_set = [(25,200),(50,100),(100,50),(200,25),(500,10),]
	# test_set = [(25,80),(50,40),(100,20),(200,10),(500,4),]
	# test_set = [(25,40),(50,20),(100,10),(200,5),(500,2),]

	# test_case = ["_model_e_5","_model_e_0_5","_model_e_0_25","_model_e_0_1",]
	# test_case = ["_model_e_0_001","_model_e_0_005",]
	# test_case = ["_model","_model_e_1","_model_e_0_1","_model_e_0_01",]
	test_case = ["_model_e_1","_model_e_0_1","_model_e_0_01",]

	f = open(folder_prefix + output + "%d_test_set.txt"%(test_set[0][0]*test_set[0][1]), "w")
	counter = 0
	for line in v_datas:
		f.write("%d\n"%counter)
		for c in line[0]:
			f.write(c)
		f.write("\n")
		for c in line[1]:
			f.write(c)
		f.write("\n")
		counter += 1
	f.close()

	N = 10
	loop = 1

	bak = sys.stdout
	for c in test_case:
		for i in xrange(N):
			testset_v = []
			for test in test_set:
				testset_v.append(verify(test, v_datas, str(test[0]*test[1]) + "_" + str(loop) + "_" + str(i) + "_", c, N))
				loop = loop + 1

			# forward stdout to file
			sys.stdout = open(folder_prefix + output + "%d_all_%s.txt"%(test_set[0][0]*test_set[0][1],c), "a+")
			for j in xrange(vlen):
				for k in xrange(len(test_set)):
					print testset_v[k][j],
				print ''
			loop = 1
			sys.stdout.close()
			sys.stdout = bak
			