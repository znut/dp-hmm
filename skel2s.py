## 2 states 6 sym
casino = {
	'states': ('F', 'L'),
	'observations': ('1','2','3','4','5','6'),
	'start_probability': {'F':0.7, 'L':0.3},
	'transition_probability': {
		'F':{'F':0.95, 'L':0.05},
		'L':{'F':0.1, 'L':0.9},
	},
	'emission_probability':{
		'F':{'1':1.0/6,'2':1.0/6,'3':1.0/6,'4':1.0/6,'5':1.0/6,'6':1.0/6},
		# 'L':{'1':0.1,'2':0.1,'3':0.1,'4':0.1,'5':0.1,'6':0.5},
		'L':{'1':0.04,'2':0.04,'3':0.04,'4':0.04,'5':0.04,'6':0.8},
	},
}

casino_log = {
	'start_probability': {'F':log(0.5), 'L':log(0.5)},
	'transition_probability': {
		'F':{'F':log(0.7), 'L':log(0.3)},
		'L':{'F':log(0.3), 'L':log(0.7)},
	},
	'emission_probability':{
		'F':{'1':log(1.0/6),'2':log(1.0/6),'3':log(1.0/6),'4':log(1.0/6),'5':log(1.0/6),'6':log(1.0/6)},
		'L':{'1':log(0.1),'2':log(0.1),'3':log(0.1),'4':log(0.1),'5':log(0.1),'6':log(0.5)},
	},	
}