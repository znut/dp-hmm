from mykov import sim,viterbi, util
import sys
from mykov.forward import ForwordAlg
from hmmpytk.hmmpytk import hmm
from math import log

casino = {
	'states': ('F', 'L'),
	'observations': ('1','2','3','4','5','6'),
	'start_probability': {'F':0.7, 'L':0.3},
	'transition_probability': {
		'F':{'F':0.95, 'L':0.05},
		'L':{'F':0.1, 'L':0.9},
	},
	'emission_probability':{
		'F':{'1':1.0/6,'2':1.0/6,'3':1.0/6,'4':1.0/6,'5':1.0/6,'6':1.0/6},
		'L':{'1':0.1,'2':0.1,'3':0.1,'4':0.1,'5':0.1,'6':0.5},
	},
}
casino_log = {
	'start_probability': {'F':log(0.7), 'L':log(0.3)},
	'transition_probability': {
		'F':{'F':log(0.95), 'L':log(0.05)},
		'L':{'F':log(0.1), 'L':log(0.9)},
	},
	'emission_probability':{
		'F':{'1':log(1.0/6),'2':log(1.0/6),'3':log(1.0/6),'4':log(1.0/6),'5':log(1.0/6),'6':log(1.0/6)},
		'L':{'1':log(0.1),'2':log(0.1),'3':log(0.1),'4':log(0.1),'5':log(0.1),'6':log(0.5)},
	},	
}

n = 2000

sss = sim.runHMM(casino,n)
sss = ('315116246446644245311321631164152133625144543631656626566666651166453132651245636664631636663162326455236266666625151631222555441666566563564324364131513465146353411126414626253356366163666466232534413661661163252562462255265252266435353336233121625364414432335163243633665562466662632666612355245242',
	   'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLLLLLLLLLFFFFFFFFFFFFLLLLLLLLLLLLLLLLFFFLLLLLLLLLLLLLLFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLLLLLLLLLLFFFFFFFFFFF')
# BOOK's VITERBI
# check_patt = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLLLLLLFFFFFFFFFFFFLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLLLLLLLLLLFFFFFFFFFFF'
# n = len(sss[0])
# print sss

# print 'ROLLS:\t',
# for i in range(n):
# 	sys.stdout.write(sss[0][i])
# print ''
# print 'REAL:\t',
# for i in range(n):
# 	sys.stdout.write(sss[1][i])
# print ''
# print 'VITERBI\t',
# v = viterbi.viterbi(casino, sss[0])
# for i in v:
# 	sys.stdout.write(i)
# print ''


# f = ForwordAlg(casino, sss[0])
# print 'FORW\t',
# for i in f.forward():
# 	sys.stdout.write(i)
# print ''

hmm_model = hmm.HMM(casino['states'], casino['observations'], casino_log['start_probability'], casino_log['transition_probability'], casino_log['emission_probability'])
# print hmm_model.viterbi(sss[0])
print 'VITERBI\t',
for i in hmm_model.viterbi(sss[0]):
	sys.stdout.write(i)
print ''
# print 'BV\t',
# print check_patt
# for i in range(n):
	# sys.stdout.write(sss[0][i])

# print v

# util.print_array(v, n, True)